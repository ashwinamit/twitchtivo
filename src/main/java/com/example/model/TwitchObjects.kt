package com.example.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by ashwin on 4/15/2016.
 */

data class TwitchPage(val self: String? = null, val next: String? = null)

interface PageableTwitchResponse {
    val _total: Int
    var _links: TwitchPage
}


data class ThumbnailBox(val large: String, val medium: String, val small: String, val template: String)

@JsonIgnoreProperties(ignoreUnknown = true)
data class GameListing(val top: Array<Game>,
                       override val _total: Int,
                       override var _links: TwitchPage) : PageableTwitchResponse {
    data class Game(val viewers: Int, val channels: Int, val game: Detail) {
        data class Detail(val name: String, val _id: Int, val box: ThumbnailBox, val logo: ThumbnailBox)

    }
}

data class StreamListing(val streams: Array<Stream>,
                         override val _total: Int,
                         override var _links: TwitchPage) : PageableTwitchResponse {
    data class Stream(val _id: Long, val channel: Channel, val preview: ThumbnailBox, val viewers: Int) {
        data class Channel(val display_name: String, val name: String, val status: String, val url: String,
                           val logo: String?)
    }

}

data class Token(val token: String, val sig: String, @JsonProperty("mobile_restricted") val mobileRestricted: String)