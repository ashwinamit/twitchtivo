package com.example.util

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.concurrent.ListenableFutureCallback
import org.springframework.web.context.request.async.DeferredResult

/**
 * Created by ashwin on 4/18/2016.
 */
abstract class AsyncSuccessHandler<T, T2>(val result: DeferredResult<T2>) : ListenableFutureCallback<ResponseEntity<T>> {
    val asyncFailure = ResponseEntity<Unit>(HttpStatus.SERVICE_UNAVAILABLE)

    companion object {
        val logger = LoggerFactory.getLogger(AsyncSuccessHandler::class.java.canonicalName)
    }

    override fun onFailure(throwable: Throwable) {
        logger.error("Task failed ${throwable.message}")
        result.setErrorResult(asyncFailure)
    }

    override fun onSuccess(tResponseEntity: ResponseEntity<T>) {
        if (tResponseEntity.hasBody()) {
            try {
                onSuccessWithBody(tResponseEntity.body)
            } catch (e: Exception) {
                logger.error("Caught exception ${e.message}")
                result.setErrorResult(asyncFailure)
            }
        } else {
            logger.error("Empty result body")
            result.setErrorResult(asyncFailure)
        }
    }

    abstract fun onSuccessWithBody(body: T)
}
