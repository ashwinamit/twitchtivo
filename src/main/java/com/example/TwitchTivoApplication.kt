package com.example;

import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.google.common.cache.CacheBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cache.guava.GuavaCache
import org.springframework.cache.support.SimpleCacheManager
import org.springframework.context.annotation.Bean
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.web.client.AsyncRestTemplate
import java.util.*
import java.util.concurrent.TimeUnit


@SpringBootApplication
@EnableCaching
open class TwitchTivoApplication {

    @Autowired(required = true)
    fun configureJackson(jackson2ObjectMapperBuilder: Jackson2ObjectMapperBuilder) {
        // needed to let jackson deserialize data class without default constructor and container
        // https://github.com/FasterXML/jackson-module-kotlin
        jackson2ObjectMapperBuilder.modulesToInstall(KotlinModule())
    }

    @Bean
    open fun cacheManager(): CacheManager {
        val simpleCacheManager = SimpleCacheManager()
        val cache = GuavaCache("twitch", CacheBuilder.newBuilder()
                .expireAfterWrite(1, TimeUnit.MINUTES).build())
        simpleCacheManager.setCaches(Arrays.asList(cache))
        return simpleCacheManager
    }

    @Bean
    open fun restTemplate(): AsyncRestTemplate {
        val template = AsyncRestTemplate()
        template.messageConverters.add(StringHttpMessageConverter())
        return template
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(TwitchTivoApplication::class.java, *args)
}