package com.example.controller

import com.example.model.GameListing
import com.example.model.StreamListing
import com.example.model.Token
import com.example.service.TwitchService
import com.example.util.AsyncSuccessHandler
import com.iheartradio.m3u8.Encoding
import com.iheartradio.m3u8.Format
import com.iheartradio.m3u8.ParsingMode
import com.iheartradio.m3u8.PlaylistParser
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.context.request.async.DeferredResult

/**
 * Created by ashwin on 4/15/2016.
 */
@Controller
class ExplorerController @Autowired constructor(val twitchService: TwitchService) {

    companion object {
        val logger = LoggerFactory.getLogger(ExplorerController::class.java.canonicalName)
    }

    @RequestMapping("/")
    fun findAll(@RequestParam(value = "limit", required = false, defaultValue = "20") limit: Int,
                @RequestParam(value = "offset", required = false, defaultValue = "0") offset: Int,
                model: Model): DeferredResult<String> {
        val result = DeferredResult<String>()
        val gamesFuture = twitchService.listGames(limit, offset)
        gamesFuture.addCallback(object : AsyncSuccessHandler<GameListing, String> (result) {
            override fun onSuccessWithBody(body: GameListing) {
                model.addAttribute("games", body.top)
                result.setResult("index")
            }
        })
        return result
    }

    @RequestMapping("/games")
    fun findGameStreams(@RequestParam(value = "game") gameName: String,
                        @RequestParam(value = "limit", required = false, defaultValue = "20") limit: Int,
                        @RequestParam(value = "offset", required = false, defaultValue = "0") offset: Int,
                        model: Model): DeferredResult<String> {
        val result = DeferredResult<String>()
        val streamsFuture = twitchService.listStreamsForGame(gameName, limit, offset)
        streamsFuture.addCallback(object : AsyncSuccessHandler<StreamListing, String> (result) {
            override fun onSuccessWithBody(body: StreamListing) {
                model.addAttribute("game", gameName)
                model.addAttribute("streams", body.streams)
                result.setResult("stream_listing")
            }
        })
        return result
    }

    @RequestMapping("/playlist")
    fun getPlaylist(@RequestParam(value = "channel") channelName: String, model: Model): DeferredResult<String> {
        val result = DeferredResult<String>()
        val tokensFuture = twitchService.getAccessToken(channelName)
        model.addAttribute("channel", channelName)
        tokensFuture.addCallback(object : AsyncSuccessHandler<Token, String>(result) {
            override fun onSuccessWithBody(body: Token) {
                logger.info("Got token for $channelName ${body.toString()}")
                val basePlaylistLink = TwitchService.getPlaylistUrl(channelName, body)
                model.addAttribute("baseLink", basePlaylistLink)
                val playlistFuture = twitchService.getPlaylist(channelName, body)
                playlistFuture.addCallback(object : AsyncSuccessHandler<String, String>(result) {
                    override fun onSuccessWithBody(body: String) {
                        val parser = PlaylistParser(body.byteInputStream(), Format.EXT_M3U, Encoding.UTF_8,
                                ParsingMode.LENIENT)
                        val playlist = parser.parse()
                        model.addAttribute("playlists", playlist.masterPlaylist.playlists)
                        result.setResult("channel_playlist")
                    }
                })
            }
        })
        return result
    }

}