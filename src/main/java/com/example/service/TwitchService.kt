package com.example.service

import com.example.model.GameListing
import com.example.model.StreamListing
import com.example.model.Token
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.CacheConfig
import org.springframework.cache.annotation.Cacheable
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.util.concurrent.ListenableFuture
import org.springframework.web.client.AsyncRestTemplate
import org.springframework.web.util.UriTemplate
import java.net.URI
import java.util.*

/**
 * Created by ashwin on 4/15/2016.
 */
@Service
@CacheConfig(cacheNames = arrayOf("twitch"))
open class TwitchService @Autowired constructor(open val template: AsyncRestTemplate) {

    companion object {
        val logger = LoggerFactory.getLogger(TwitchService::class.java.canonicalName)
        val random = Random()
        val GAME_DIRECTORY_URL = "https://api.twitch.tv/kraken/games/top"
        val STREAM_DIRECTORY_URL = "https://api.twitch.tv/kraken/streams"

        fun getGameDirectoryUrl(limit: Int, offset: Int) = "$GAME_DIRECTORY_URL?offset=$offset&limit=$limit"
        fun getSreamDirectoryUrl(game: String, limit: Int, offset: Int) =
                "$STREAM_DIRECTORY_URL?game=$game&offset=$offset&limit=$limit"

        fun getTokenUrl(channelName: String) = "http://api.twitch.tv/api/channels/$channelName/access_token"
        fun getPlaylistUrl(channelName: String, token: Token): URI {
            val url = UriTemplate("http://usher.twitch.tv/api/channel/hls/$channelName.m3u8?player=twitchweb&token={token}&sig={sig}&allow_audio_only=true&allow_source=true&type=any&p={nonce}")
                    .expand(token.token, token.sig, random.nextInt(10000).toString())
            return url
        }
    }

    @Cacheable
    open fun listGames(limit: Int, offset: Int): ListenableFuture<ResponseEntity<GameListing>> {
        val twitchUrl = getGameDirectoryUrl(limit, offset)
        logger.info("About to download: $twitchUrl")
        val results: ListenableFuture<ResponseEntity<GameListing>> =
                template.getForEntity(twitchUrl, GameListing::class.java)
        return results
    }

    @Cacheable
    open fun listStreamsForGame(game: String, limit: Int, offset: Int):
            ListenableFuture<ResponseEntity<StreamListing>> {
        val streamDirectoryUrl = getSreamDirectoryUrl(game, limit, offset)
        logger.info("About to download $streamDirectoryUrl}")
        val results: ListenableFuture<ResponseEntity<StreamListing>> =
                template.getForEntity(streamDirectoryUrl, StreamListing::class.java)
        return results
    }

    @Cacheable
    open fun getPlaylist(channelName: String, token: Token): ListenableFuture<ResponseEntity<String>> {
        val url = getPlaylistUrl(channelName, token)
        logger.info("Downloading playlist at ${url.toASCIIString()}")
        return template.getForEntity(url, String::class.java)
    }

    @Cacheable
    open fun getAccessToken(channelName: String) = template.getForEntity(getTokenUrl(channelName), Token::class.java)
}